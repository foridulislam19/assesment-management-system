﻿using RepositoryPattern.Abstractions.BLL;
using RepositoryPattern.Abstractions.Repository;
using RepositoryPattern.Abstractions.Repository.Base;
using RepositoryPattern.BLL.Base;
using RepositoryPattern.Models;

namespace RepositoryPattern.BLL
{
    public class DeparmentManager : Manager<Department>, IDepartmentManager
    {
        private readonly IDeparmentRepository _repository;

        public DeparmentManager(IDeparmentRepository repository) : base(repository)
        {
        }
    }
}