﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using RepositoryPattern.Abstractions.BLL;
using RepositoryPattern.Models;
using RepositoryPattern.ServerDbContext;

namespace RepositoryPattern.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DepartmentsController : ControllerBase
    {
        private readonly IDepartmentManager _department;

        public DepartmentsController(IDepartmentManager department)
        {
            _department = department;
        }

        // GET: api/Departments
        [HttpGet]
        public async Task<ICollection<Department>> GetDepartments()
        {
            return await _department.GetAll();
        }

        // GET: api/Departments/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Department>> GetDepartment(long id)
        {
            var department = await _department.GetById(id);

            if (department == null)
            {
                return NotFound();
            }

            return department;
        }

        // PUT: api/Departments/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutDepartment(long id, Department department)
        {
            if (id != department.Id)
            {
                return BadRequest("Not Found!");
            }
            var aDepartment = await _department.GetById(id);
            try
            {
                await _department.Update(aDepartment);
            }
            catch (DbUpdateConcurrencyException)
            {
                return BadRequest();
            }

            return NoContent();
        }

        // POST: api/Departments
        [HttpPost]
        public async Task<ActionResult<Department>> PostDepartment(Department department)
        {
            if (ModelState.IsValid)
            {
                bool isAdded = await _department.Add(department);
                if (isAdded)
                {
                    return CreatedAtAction("GetDepartment", new { id = department.Id }, department);
                }
            }

            return department;
        }

        // DELETE: api/Departments/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Department>> DeleteDepartment(long id)
        {
            var department = await _department.GetById(id);
            if (department == null)
            {
                return NotFound();
            }

            var isdeleted = await _department.Remove(department);
            if (isdeleted)
            {
                return Ok();
            }

            return department;
        }
    }
}