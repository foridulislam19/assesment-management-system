﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using RepositoryPattern.Abstractions.Repository;
using RepositoryPattern.Models;
using RepositoryPattern.Repositories.Base;
using RepositoryPattern.ServerDbContext;

namespace RepositoryPattern.Repositories
{
    public class DepartmentRepository : EfRepository<Department>, IDeparmentRepository
    {
        private readonly UniversityDb _universityDb;

        public DepartmentRepository(DbContext db) : base(db)
        {
            _universityDb = db as UniversityDb;
        }
    }
}