﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using RepositoryPattern.Abstractions.Repository.Base;

namespace RepositoryPattern.Repositories.Base
{
    public class EfRepository<T> : IRepository<T> where T : class
    {
        private readonly DbContext _db;

        public EfRepository(DbContext db)
        {
            _db = db;
        }

        public virtual async Task<bool> Add(T entity)
        {
            _db.Set<T>().Add(entity);
            return await _db.SaveChangesAsync() > 0;
        }

        public virtual async Task<bool> Remove(T entity)
        {
            _db.Set<T>().Remove(entity);
            return await _db.SaveChangesAsync() > 0;
        }

        public virtual async Task<bool> Update(T entity)
        {
            _db.Set<T>().Update(entity);
            return await _db.SaveChangesAsync() > 0;
        }

        public virtual async Task<ICollection<T>> GetAll()
        {
            return await _db.Set<T>().ToListAsync();
        }

        public virtual async Task<T> GetById(long id)
        {
            return await _db.Set<T>().FindAsync(id);
        }
    }
}