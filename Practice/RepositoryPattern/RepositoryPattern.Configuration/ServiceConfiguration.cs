﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using RepositoryPattern.Abstractions.BLL;
using RepositoryPattern.Abstractions.Repository;
using RepositoryPattern.BLL;
using RepositoryPattern.Repositories;
using RepositoryPattern.ServerDbContext;

namespace RepositoryPattern.Configuration
{
    public class ServiceConfiguration
    {
        public static void ServiceConfigure(IServiceCollection service)
        {
            service.AddTransient<IDepartmentManager, DeparmentManager>();
            service.AddTransient<IDeparmentRepository, DepartmentRepository>();
            service.AddTransient<DbContext, UniversityDb>();
            service.AddTransient<UniversityDb>();
        }
    }
}