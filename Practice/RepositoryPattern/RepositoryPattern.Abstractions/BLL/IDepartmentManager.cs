﻿using RepositoryPattern.Abstractions.BLL.Base;
using RepositoryPattern.Models;

namespace RepositoryPattern.Abstractions.BLL
{
    public interface IDepartmentManager : IManager<Department>
    {
    }
}