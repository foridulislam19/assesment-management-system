﻿using RepositoryPattern.Abstractions.Repository.Base;
using RepositoryPattern.Models;

namespace RepositoryPattern.Abstractions.Repository
{
    public interface IDeparmentRepository : IRepository<Department>
    {
    }
}