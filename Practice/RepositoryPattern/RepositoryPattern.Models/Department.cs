﻿namespace RepositoryPattern.Models
{
    public class Department
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
    }
}