﻿using System.Security.Principal;
using Microsoft.EntityFrameworkCore;
using RepositoryPattern.Models;

namespace RepositoryPattern.ServerDbContext
{
    public class UniversityDb : DbContext
    {
        public UniversityDb(DbContextOptions<UniversityDb> options) : base(options)
        {
        }

        public DbSet<Department> Departments { get; set; }
    }
}