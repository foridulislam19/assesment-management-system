import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class StudentService {

  constructor(private http: HttpClient) { }
  getStudentList() {
    return this.http.get(environment.baseApiURL + 'students');
  }
  postStudents(formdata) {
    return this.http.post(environment.baseApiURL + 'students', formdata);
  }
  putStudents(formdata) {
    return this.http.put(environment.baseApiURL + 'students/' + formdata.id, formdata);
  }
  deleteStudents(id){
    return this.http.delete(environment.baseApiURL + 'students/' + id);
  }
}
