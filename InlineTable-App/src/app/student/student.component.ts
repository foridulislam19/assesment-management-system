import { StudentService } from './../shared/student.service';
import { DepartmentService } from './../shared/department.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormArray, Validators, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-student',
  templateUrl: './student.component.html',
  styleUrls: ['./student.component.css']
})
export class StudentComponent implements OnInit {

  constructor(
    private departList: DepartmentService,
    private fb: FormBuilder,
    private stidentSer: StudentService) { }
  deptLists = [];
  stdudentForms: FormArray = this.fb.array([]);
  ngOnInit() {
    this.departmentList();
    this.getStudentList();
  }
  departmentList() {
    this.departList.getDepartment().subscribe(response => {
      this.deptLists = response as [];
    });
  }
  getStudentList() {
    this.stidentSer.getStudentList().subscribe(res => {
      if (res == []) {
        this.addStudentsForm();
      } else {
        (res as []).forEach((student: any) => {
          this.stdudentForms.push(this.fb.group({
            id: [student.id],
            name: [student.name, Validators.required],
            regNo: [student.regNo, Validators.required],
            departmentId: [student.departmentId, Validators.required]
          }));
        });
      }
    });
  }
  addStudentsForm() {
    this.stdudentForms.push(this.fb.group({
      id: [0],
      name: ['', Validators.required],
      regNo: ['', Validators.required],
      departmentId: ['', Validators.required]
    }));
  }
  recordSubmit(fg: FormGroup) {
    if (fg.value.id == 0) {
      this.stidentSer.postStudents(fg.value).subscribe(
        (res: any) => {
          fg.patchValue({ id: res.id });
        });
    } else {
      this.stidentSer.putStudents(fg.value).subscribe(
        (res: any) => {
        });
    }
  }
  onDelete(id, i) {
    if (id == 0) {
      this.stdudentForms.removeAt(i);
    } else if (confirm('Are you sure to delete this record!')) {
      this.stidentSer.deleteStudents(id).subscribe(
        res => {
          this.stdudentForms.removeAt(i);
        }
      );
    }
  }

}
